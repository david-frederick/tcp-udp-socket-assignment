# UDP/TCP Socket Assignment #

This assignment required me to create two applications which would communicate through a socket connection (using both TCP and UDP). The client application connects to the server and then sends a text file. The server receives the file and then returns a score based on the number of 'illegal' words found in the file.

I used python 2.7 for this assignment.

### How do I run this? ###

Firstly, you need Python 2.7 installed. Then follow these steps:

* Checkout the project using git.
* Start the server application: `cmd> python smsengineUDP.py 8591 suspicious-words.txt`
* Run the client application: `cmd> python smsclientUDP.py 127.0.0.1 8591 msg.txt`
 
The above commands will run the UDP applications. To run using TCP, replace "UDP" in the filenames with "TCP".

### Code usage guidelines ###

This code is protected under the [MIT Liscence](https://choosealicense.com/licenses/mit/).