import sys
from socket import *

def main():
  serverPort = int(sys.argv[1])
  filepath = sys.argv[2]
  illegal = illegalWords(filepath)
  print "server using port number: " + str(serverPort)
  print "server illegal word file: " + str(filepath) + "\n"
  serverSocket = socket(AF_INET, SOCK_DGRAM)
  serverSocket.bind(('', serverPort))
  print "The server is ready to receive\n"

  while 1:
    message, clientAddress = serverSocket.recvfrom(2048)
    print "received message: ", message
    response = createResponse(message, illegal)
    serverSocket.sendto(str(response), clientAddress)
    print "sent response: " + str(response) + "\n"

# gets a list of illegal words given a filepath
def illegalWords(filepath):
  file = open(filepath, 'r')
  illegal = file.read()
  file.close()
  return illegal.split("\n")

def createResponse(message, illegal):
  try:
    message = message.split(" ")
    illCount = 0
    totCount = 0
    illWords = []

    for entry in message:
      totCount += 1
      if entry in illegal:
        illCount += 1
        illWords.append(entry)

    spamScore = float(illCount)/totCount
    illWords = ' '.join(map(str,illWords))
    response = "" + str(illCount) + " " + str(spamScore) + " " + illWords
  except StandardError:
    response = "0 -1 ERROR"
  finally:
    return response

main()