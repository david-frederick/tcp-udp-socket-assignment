import sys
import socket

def main():
  serverIp = sys.argv[1]
  serverPort = int(sys.argv[2])
  filepath = sys.argv[3]

  file = open(filepath, 'r')
  message = file.read()
  file.close()

  clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  clientSocket.connect((serverIp,serverPort))
  clientSocket.send(message)
  modifiedSentence = clientSocket.recv(1024)
  print "From Server:", modifiedSentence
  clientSocket.close()

main()