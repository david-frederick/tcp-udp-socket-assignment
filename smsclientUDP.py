import sys
import time
import socket
import traceback

def main():
  serverIp = sys.argv[1]
  serverPort = int(sys.argv[2])
  filepath = sys.argv[3]

  file = open(filepath, 'r')
  message = file.read()
  file.close()

  print "sending message found in: " + str(filepath)
  print "to the server at: " + str(serverIp) + ":" + str(serverPort) + "\n"
  clientSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  clientSocket.settimeout(0.1)

  attempts = 0
  while (attempts <= 2):
    try:
      clientSocket.sendto(message, (serverIp, serverPort))
      checkResponse(clientSocket)
      break
    except StandardError:
      if(attempts==2):
        raise StandardError("The server has failed to respond. No more attempts will be made.")
      else:
        print "The server has not answered in the last two seconds.\nRetrying...\n"
        attempts += 1

  clientSocket.close()

# prints a response as soon as it is received.
# if no response is received after 2 seconds, raises an exception.
def checkResponse(socket):
  # give up retrying after 2 seconds
  startTime = time.time()
  elapsedTime = time.time() - startTime
  while (elapsedTime < 2):
    try:
      # check for response
      modifiedMessage, serverAddress = socket.recvfrom(2048)
      break
    except StandardError as e:
      elapsedTime = time.time() - startTime

  print "message received: ", modifiedMessage
  print "from location: " + str(serverAddress[0]) + ":" + str(serverAddress[1])

main()